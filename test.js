const memoization = require('./memoizaton');
const expect = require('chai').expect;
const sinon = require('sinon')

describe('memoization', function () {
    it('should memoize function result', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);

        returnValue = 10;

        // TODO currently fails, should work after implementing the memoize function, it should also work with other
        // types then strings, if there are limitations to which types are possible please state them
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);
    });

    it('using types other than string: Object', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized( {'key': 5} )).to.equal(5);

        returnValue = 10;

        expect(memoized( {'key': 5} )).to.equal(5);
    });

    it('using types other than string: boolean', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized( false )).to.equal(5);

        returnValue = 10;

        expect(memoized( false )).to.equal(5);
    });

    it('using types other than string: number', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized( 10.0 )).to.equal(5);

        returnValue = 10;

        expect(memoized( 10.0 )).to.equal(5);
    });

    it('using types other than string: undefined', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized( undefined )).to.equal(5);

        returnValue = 10;

        expect(memoized( undefined )).to.equal(5);
    });

    it('using types other than string: null', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized( null )).to.equal(5);

        returnValue = 10;

        expect(memoized( null )).to.equal(5);
    });

    
    it('using types other than string: Symbol', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        let sym = Symbol('foo')
        
        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized( sym )).to.equal(5);

        returnValue = 10;

        expect(memoized( sym )).to.equal(5);
    });

    it('after timeout func is computed again', () =>{
        const clock = sinon.useFakeTimers();
        let returnValue = 5;
        const testFunction =  (key) => returnValue;
        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);

        returnValue = 10;

        clock.tick(999); //timeout yet to be passed
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);

        clock.tick(1);  //timeout passed
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(10);

        returnValue = 15;

        clock.tick(1000);  //cache is not cleared again
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(10);

        clock.restore();
    });
});